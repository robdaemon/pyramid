type pip = Ace | Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten |
           Jack | Queen | King
let pips = [Ace; Two; Three; Four; Five; Six; Seven; Eight; Nine; Ten;
            Jack; Queen; King]

type suit = Diamonds | Spades | Hearts | Clubs
let suits = [Diamonds; Spades; Hearts; Clubs]

type card = pip * suit

let full_deck =
  Array.of_list (List.concat (List.map (fun pip -> List.map (fun suit -> (pip, suit)) suits) pips))

let shuffle deck =
  for i = Array.length deck - 1 downto 1 do
    let j = Random.int (i+1) in
    let temp = deck.(i) in
    deck.(i) <- deck.(j);
    deck.(j) <- temp
  done

let card_pip (c: card) =
  match fst c with
    Ace -> "A"
  | Two -> "2"
  | Three -> "3"
  | Four -> "4"
  | Five -> "5"
  | Six -> "6"
  | Seven -> "7"
  | Eight -> "8"
  | Nine -> "9"
  | Ten -> "T"
  | Jack -> "J"
  | Queen -> "Q"
  | King -> "K"

let card_suit (c: card) =
  match snd c with
  | Diamonds -> "D"
  | Spades -> "S"
  | Hearts -> "H"
  | Clubs -> "C"

let print_card (c: card) =
  print_string (card_pip c);
  print_string (card_suit c);
  print_newline ()

let () =
  Random.init (int_of_float (Unix.time ()));
  let deck = full_deck in
  shuffle deck;
  Array.iter print_card deck;
